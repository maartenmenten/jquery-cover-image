(function()
{
	function Plugin( elem, options )
	{
		this.$elem   = jQuery( elem );
		this.options = jQuery.extend( {}, Plugin.defaultOptions, options );

		this.$elem.addClass( 'cover-image' );

		this.$item = this.$elem.find( this.options.item );
		this.$item.addClass( 'cover-image-item' );

		// Sets ratio class when not already added.
		if ( this.options.ratio ) 
		{
			var classes = this.$elem.attr( 'class' );

			if ( ! /cover-image-\d+by\d+/.test( classes ) )
			{
				this.$elem.addClass( 'cover-image-' + this.options.ratio );
			};
		};

		Plugin.register( this );

		jQuery( document ).trigger( 'coverImage/init', [ this ] );

		this.update();
	}

	Plugin.defaultOptions =
	{
		item  : 'img, .cover-image-item',
		ratio : '4by3'
	};

	Plugin.instances = [];

	Plugin.init = function()
	{
		jQuery( window ).on( 'load', function( event )
		{
			Plugin.update();
		});

		jQuery( window ).on( 'resize', function( event )
		{
			Plugin.waitForFinalEvent( Plugin.update, 250, 'update' );
		});
	};

	// TODO : set key
	Plugin.register = function( instance )
	{
		if ( ! Plugin.instances.length ) 
		{
			Plugin.init();
		};

		Plugin.instances.push( instance );
	};

	Plugin.update = function()
	{
		jQuery.each( Plugin.instances, function()
		{
			this.update();
		});
	};

	Plugin.waitForFinalEvent = (function()
	{
		var timers = {};
		
		return function ( callback, ms, uniqueId ) 
		{
			if ( ! uniqueId )
			{
				uniqueId =  "Don't call this twice without a uniqueId";
			};

			if ( timers[ uniqueId ] )
			{
				clearTimeout ( timers[ uniqueId] );
			};

			timers[ uniqueId ] = setTimeout( callback, ms );
		};
	})();

	Plugin.prototype.update = function() 
	{
		console.log( 'update' );

		this.$item.removeClass( 'parent-wider parent-taller' );

		if ( this.$item.width() < this.$elem.width() ) 
		{
			this.$elem.addClass( 'cover-image-x' );
		}

		if ( this.$item.height() < this.$elem.height() )
		{
			this.$elem.addClass( 'cover-image-y' );
		}
	};

	/**
	 * jQuery Function
	 */
	jQuery.fn.coverImage = function( options )
	{
		return this.each( function()
		{
			if ( jQuery( this ).data( 'coverImage' ) ) 
			{
				return true;
			}

			var instance = new Plugin( this, options );

			jQuery( this ).data( 'coverImage', instance );

		});
	};

	window.coverImage = Plugin;

})();
